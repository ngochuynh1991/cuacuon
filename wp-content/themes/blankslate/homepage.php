<?php
/*
  Template Name: homepage
 */

 $cat_home_1 = 12; // Cua chong muoi
 $cat_home_2 = 13; // Gian phoi thong minh
 $cat_home_3 = 14; // san pham khác
 $cat_home_4 = 10; // Tin tức
 //$cat_home_5 = 9; // Công trình đã thi công
 $limit = 6;
 $limit_box = 5;
?>
<?php get_header(); ?>
<?php get_sidebar(); ?>
<section id="content" class="content">
    
    <div class="feature">
        <div class="entry-title">
            <h2><?php echo get_cat_name($cat_home_1); ?></h2>
        </div>
        <div class="box-body">
            <ul class="product">
            <?php 
            $query = new WP_Query( 'cat='.$cat_home_1.'&posts_per_page='.$limit);
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
				get_template_part( 'template-parts/list-product' );
			endwhile; 
			wp_reset_postdata();
            else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
            </ul>
        </div>
    </div>
	<div class="feature">
        <div class="entry-title">
            <h2><?php echo get_cat_name($cat_home_2); ?></h2>
        </div>
        <div class="box-body">
            <ul class="product">
            <?php 
            $query = new WP_Query( 'cat='.$cat_home_2.'&posts_per_page='.$limit);
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
				get_template_part( 'template-parts/list-product' );
			endwhile; 
			wp_reset_postdata();
            else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
            </ul>
        </div>
    </div>
	<div class="feature">
        <div class="entry-title">
            <h2><?php echo get_cat_name($cat_home_3); ?></h2>
        </div>
        <div class="box-body">
            <ul class="product">
            <?php 
            $query = new WP_Query( 'cat='.$cat_home_3.'&posts_per_page='.$limit);
            if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
				get_template_part( 'template-parts/list-product' );
			endwhile; 
			wp_reset_postdata();
            else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
            </ul>
        </div>
    </div>
	<div class="split_home">
                <div class="entry-title">
                        <h2><?php echo get_cat_name($cat_home_4); ?></h2>
                </div>
                <div class="col-6" style="width: 100%;">
                    <ul class="news box_body">
                    <?php 
                    $query = new WP_Query( 'cat='.$cat_home_4.'&posts_per_page='.$limit);
                    if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>">
                            <span><?php the_title(); ?></span>
                        </a>
                    </li>
                    <?php
                    endwhile; 
                    wp_reset_postdata();
                    endif; ?>
                    </ul>
		</div>
	</div>
	<div class="clear"></div>
	
</section>
<?php get_footer(); ?>