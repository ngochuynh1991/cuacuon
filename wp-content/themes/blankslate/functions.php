<?php
add_action( 'after_setup_theme', 'blankslate_setup' );
function blankslate_setup()
{
load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
);
}
add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts()
{
wp_enqueue_script( 'jquery' );
}
add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'blankslate_filter_wp_title' );
function blankslate_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'blankslate_widgets_init' );
function blankslate_widgets_init()
{
register_sidebar( array (
    'name' => __( 'Footer Widget Area', 'blankslate' ),
    'id' => 'primary-widget-area',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );

register_sidebar( array (
    'name' => __( 'Tư vấn trực tiếp', 'blankslate' ),
    'id' => 'phone-widget-area',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '',
    'after_title' => '',
) );
}
function blankslate_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php 
}
add_filter( 'get_comments_number', 'blankslate_comments_number' );
function blankslate_comments_number( $count )
{
return $count;
}

function get_post_count($cat_id) {
    global $wpdb;

    $post_count = 0;
    $querystr = "SELECT count FROM $wpdb->term_taxonomy WHERE term_id = $cat_id";
    $result = $wpdb->get_var($querystr);
    $post_count += $result;

  return $post_count;
}
function custom_meta_box_html($post) {
    $meta_price = get_post_meta($post->ID, 'meta_price');
    $price = !empty($meta_price[0]) ? $meta_price[0] : '';
    
    $meta_info = get_post_meta($post->ID, 'meta_info');
    $info = !empty($meta_info[0]) ? $meta_info[0] : '';
    ?>
    <table id="tblCusBox" class="cusbox">
        <tr class="row-cusbox">
            <td><label>Giá: </label></td>
            <td><input id="txt_price" name="txt_price" value="<?php echo $price; ?>" /></td>
            <td style="font-size: 14px;">.000 VNĐ</td>
        </tr>
        <tr class="row-cusbox">
            <td colspan="3"><label>Thông số kĩ thuật: </label></td>
        </tr>
        <tr class="row-cusbox">
            <td colspan="3"><textarea name="meta_info" rows="7"><?php echo $info; ?></textarea></td>
        </tr>
    </table>
    <style type="text/css">
        .cusbox{border: 0px;}
        .row-cusbox{padding: 10px;}
        .row-cusbox label{font-weight: bold;}
        .row-cusbox input{
            border-radius: 2px;
            border: 1px solid #dadada;
            padding: 3px 5px;
            text-align: right;
            max-width: 140px;
        }
        .row-cusbox textarea {width: 100%;}
    </style>
    <?php
}

function add_custom_meta_box() {
    add_meta_box("custom-meta-box", "Thông số", "custom_meta_box_html", "post", "side", "high", null);
}
add_action("add_meta_boxes", "add_custom_meta_box");

function save_custom_meta_box($post_id, $post, $update) {
    $post_id = get_the_ID();
    if (!current_user_can("edit_post", $post_id))
        return $post_id;
    if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;
    $slug = "post";
    if ($slug != $post->post_type)
        return $post_id;

    $price = isset($_POST["txt_price"]) ? $_POST["txt_price"] : '';
    if (!empty($price)) {
        update_post_meta($post_id, "meta_price", $price);
    }
    
    $info = isset($_POST["meta_info"]) ? $_POST["meta_info"] : '';
    if (!empty($info)) {
        update_post_meta($post_id, "meta_info", $info);
    }
}
add_action("save_post", "save_custom_meta_box", 10, 3);

function process_info($content) {
	if($content == '') return '';
    $str = '';
    $count = 1;
    $content = preg_replace("/<(p|br|div)[^>]*?(\/?)>/i",'<$1>', $content);
    $content = str_replace('<p></p>','',$content);
    $content = str_replace('<p><p>','',$content);
    $content = str_replace('</p></p>','',$content);
    $content = str_replace('<div>','<p>',$content);
    $content = str_replace('</div>','</p>',$content);
    $content = str_replace('<h3>','',$content);
    $content = str_replace('</h3>','',$content);
    $content_temp = preg_split("/\r\n/",$content);
    
    foreach ($content_temp as $key => $content_p) {
        if(!empty($content_p) &&( strpos($content_p,'<p>') === false && strpos($content_p,'</p>') ===false)) {
            $str .= "<p>".$content_p."</p>";
        }
        $count++;
    }
    return $str;
}