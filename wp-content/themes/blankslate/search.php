<?php get_header(); ?>
<?php get_sidebar(); ?>
<section id="content" class="content">
    <?php if ( have_posts() ) : ?>
    <div class="entry-title">
        <h2><?php printf( __( 'Search Results for: %s', 'blankslate' ), get_search_query() ); ?></h2>
    </div>
    <div class="box-body">
        <ul class="product">
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part('template-parts/list-product'); ?>
            <?php endwhile; ?>
        </ul>
    </div>
    <?php get_template_part( 'nav', 'below' ); ?>
    <?php else : ?>
    <article id="post-0" class="post no-results not-found">
        <div class="entry-title">
            <h2><?php _e( 'Nothing Found', 'blankslate' ); ?></h2>
        </div>
        <section class="box-body">
            <p><?php _e( 'Sorry, nothing matched your search. Please try again.', 'blankslate' ); ?></p>
        </section>
    </article>
    <?php endif; ?>
</section>
<?php get_footer(); ?>