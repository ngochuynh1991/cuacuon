<aside id="sidebar" class="sidebar fl">
	<div class="box">
        <div class="title"><a href="javascript:void(0)">Hỗ trợ khách hàng</a></div>
        <ul class="box_body">
            <li class="support">
                <img src="<?php echo get_template_directory_uri(); ?>/images/ho_tro_kh.png" width="198" height="166" />
                <!--<p class="hot_line">Hot Line: 0965.832.919</p>-->
            </li>
        </ul>
    </div>
    
    <div class="box">
        <div class="title">
            <a href="javascript:void(0)">Hỗ trợ trực tuyến</a>
        </div>
        <div class="box_body">
            <div class="fb-page"
                data-href="https://www.facebook.com/HoiNhungNguoiDiHocBangXeBus/" 
                data-hide-cover="false" 
				data-width="198" 
                data-show-facepile="true">
            </div>
        </div>
    </div>
	<div class="box">
        <?php 
        $cat_id = 9;
        $args = array(
            'posts_per_page'   => 10,
            'offset'           => 0,
            'category'         => $cat_id,
            'category_name'    => '',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'include'          => '',
            'exclude'          => '',
            'meta_key'         => '',
            'meta_value'       => '',
            'post_type'        => 'post',
            'post_mime_type'   => '',
            'post_parent'      => '',
            'author'	   => '',
            'post_status'      => 'publish',
            'suppress_filters' => true 
        );
        $myposts = get_posts($args); 
        ?>
        <div class="title">
            <a href="<?php echo get_category_link($cat_id); ?>"><?php echo get_cat_name($cat_id);?></a>
        </div>
        <ul class="menu_sidebar box_body">
            <?php foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
            <li>
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </li>
            <?php endforeach;                    
            wp_reset_postdata(); ?>
        </ul>
    </div>
	
    <div class="box">
        <?php 
        $cat_news_id = 25; //mẹo hay
        $arg_news = array(
            'posts_per_page'   => 10,
            'offset'           => 0,
            'category'         => $cat_news_id,
            'category_name'    => '',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'include'          => '',
            'exclude'          => '',
            'meta_key'         => '',
            'meta_value'       => '',
            'post_type'        => 'post',
            'post_mime_type'   => '',
            'post_parent'      => '',
            'author'	   => '',
            'post_status'      => 'publish',
            'suppress_filters' => true 
        );
        $news = get_posts($arg_news);
        ?>
        <div class="title">
            <a href="<?php echo get_category_link($cat_news_id); ?>"><?php echo get_cat_name($cat_news_id);?></a>
        </div>
        <ul class="news box_body">
            <?php foreach ($news as $post) : setup_postdata($post); ?>
            <li>
				<a href="<?php the_permalink(); ?>">
                    <?php 
                    if (has_post_thumbnail()) {
                        the_post_thumbnail(array(60,50));
                    } else {
                    ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/no_image.png" width="60" />
                    <?php
                    }
                    ?>
                    <span><?php the_title(); ?></span>
                </a>
            </li>
            <?php endforeach;                    
            wp_reset_postdata(); ?>
        </ul>
    </div>
</aside>

