<?php get_header(); ?>
<?php get_sidebar(); ?>
<section id="content" class="content">
    <div class="entry-title">
        <h2><?php echo get_the_category($post->ID)[0]->name; ?></h2>
    </div>
    
    <?php if(get_the_category($post->ID)[0]->term_id == 10):
        get_template_part('template-parts/single-news');
    else: 
        get_template_part('template-parts/single-product');
    endif; 
    ?>
</section>
<?php get_footer(); ?>