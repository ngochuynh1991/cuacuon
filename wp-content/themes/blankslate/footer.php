</div><!-- END MAIN -->	
    <footer class="footer">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 95%;">
            <tbody>
                <tr>
                    <td>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" style="margin-left: 20px;">
                            <img src="<?php bloginfo('template_directory'); ?>/images/logo2.png" width="186" height="105" />
                        </a>
                    </td>
                    <td>
                        <?php if ( is_active_sidebar( 'primary-widget-area' ) ) : 
                            dynamic_sidebar( 'primary-widget-area' ); 
                        endif; ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </footer>
    <div id="toTop">^ Về đầu trang</div>
    <div id="BottomLayer">
        <h2 class="bb">
            Tư vấn trực tiếp
            <span class="icon min"></span>
        </h2>
        <div class="clearfix">
            <?php if ( is_active_sidebar( 'phone-widget-area' ) ) : 
                dynamic_sidebar( 'phone-widget-area' ); 
            endif; ?>
        </div>
    </div>
    <div class="adv adv1">
        <img src="<?php bloginfo('template_directory'); ?>/images/adv1.png" width="150" />
    </div>
    <div class="adv adv2">
        <img src="<?php bloginfo('template_directory'); ?>/images/adv2.png" width="150" />
    </div>
    <script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() != 0) { 
                jQuery('#toTop').fadeIn(); 
            } else { 
                jQuery('#toTop').fadeOut(); 
            } 
        }); 
        jQuery('#toTop').click(function () {
            jQuery('body,html').animate({ scrollTop: 0 }, 800); 
        });

        var icon = "#BottomLayer .icon";
        jQuery(icon).click(function() {
            jQuery("#BottomLayer .clearfix").slideToggle( "fast", function() {
                if(jQuery(icon).hasClass( "min" )) {
                    jQuery(icon).removeClass('min').addClass('max');
                } else {
                    jQuery(icon).removeClass('max').addClass('min');
                }
            });
        });
    });
    </script>
    </div>
<?php wp_footer(); ?>
</body>
</html>