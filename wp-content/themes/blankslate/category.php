<?php get_header(); ?>
<?php get_sidebar(); 
$category_id = get_the_category()[0]->term_id;
?>
<section id="content" class="content">
    <div class="entry-title">
        <h2><?php single_cat_title(); ?></h2>
    </div>
    <div class="box-body">
        <div class="desciption"> <?php echo category_description(); ?> </div>
        <div class="clear"></div>
        <ul class="<?php echo ($category_id == 10) ? "box_news news" : "product"; ?>">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'template-parts/list-product' ); ?>
            <?php endwhile; endif; ?>
        </ul>
    </div>
    <?php get_template_part( 'nav', 'below' ); ?>
</section>
<?php get_footer(); ?>