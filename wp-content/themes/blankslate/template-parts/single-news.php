<div class="detail_news box-body">
    <header>
        <h1><?php the_title(); ?></h1>
    </header>
    <div class="content_news">
        <?php the_content(); ?>
    </div>
</div>
<div class="comment_facebook">
    <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-numposts="5" width="755"></div>
</div>
<div class="other_news">
    <h3>Bài mới hơn</h3>
    <div>
        <?php 
        $post_id = get_the_ID();
        $cat_news_id = 10; 
        $arg_news = array(
            'posts_per_page'   => 4,
            'offset'           => 0,
            'category'         => $cat_news_id,
            'category_name'    => '',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'include'          => '',
            'exclude'          => '',
            'meta_key'         => '',
            'meta_value'       => '',
            'post_type'        => 'post',
            'post_mime_type'   => '',
            'post_parent'      => '',
            'author'	   => '',
            'post_status'      => 'publish',
            'suppress_filters' => true 
        );
        $news = get_posts($arg_news);
        ?>
        <ul>
            <?php foreach ($news as $post) : setup_postdata($post); 
            if(get_the_ID() != $post_id):
            ?>
            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php endif; endforeach;                    
            wp_reset_postdata(); ?>
        </ul>
    </div>
</div>
