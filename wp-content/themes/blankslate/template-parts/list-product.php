<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <a href="<?php the_permalink(); ?>">
		<?php 
		if (has_post_thumbnail()) {
			the_post_thumbnail();
		} else {
		?>
		<img src="<?php echo get_template_directory_uri(); ?>/images/no_image.png" />
		<?php
		}
		?>
        <h3><?php the_title(); ?></h3>
    </a>
</li>