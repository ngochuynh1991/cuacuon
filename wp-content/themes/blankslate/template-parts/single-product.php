<div class="box-body">
    <div class="detail_product">
        <div class="box_img">
            <a class="example-image-link" 
               href="<?php echo get_the_post_thumbnail_url(); ?>" 
               data-lightbox="example-1">
                <?php the_post_thumbnail(); ?>
            </a>
        </div>
        <div class="info">
            <?php 
            $meta_price = get_post_meta(get_the_ID(), 'meta_price');
            $price = !empty($meta_price[0]) ? $meta_price[0].'.000 VNĐ' : 'Liên hệ';
            
            $meta_info = get_post_meta($post->ID, 'meta_info');
            $info = !empty($meta_info[0]) ? $meta_info[0] : '';
            $content = !empty($info) ? process_info($info) : '';
            ?>
			<h1><?php the_title(); ?></h1>
            <!--<h4>Thông số kĩ thuật sản phẩm </h4>-->
            <div><?php echo $content; ?></div>
            <p class="price">Giá bán: <span class="red"><?php echo $price; ?></span></p>
            <div class="buttonCart">
<!--                <a href="/cart/id" title="Đặt mua">Đặt mua<span>&nbsp;</span></a>-->
                <a href="<?php echo esc_url( home_url( '/contact' ) ); ?>" title="Đặt mua">Đặt mua<span>&nbsp;</span></a>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="entry-content">
		<div class="entry-title">
			<h2>Thông số kĩ thuật sản phẩm</h2>
		</div>
        <?php the_content(); ?>
    </div>
</div>
<div class="related_post">
    <div class="entry-title">
        <h2>Sản phẩm cùng loại</h2>
    </div>
    <div class="box-body">
        <?php 
        $cats  = get_the_category($post->ID);
        $cat_id = $cats[0]->term_id;
        ?>
        <ul class="product">
        <?php 
        $args = array(
            'posts_per_page'   => 9,
            'offset'           => 0,
            'category'         => $cat_id,
            'category_name'    => '',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'include'          => '',
            'exclude'          => '',
            'meta_key'         => '',
            'meta_value'       => '',
            'post_type'        => 'post',
            'post_mime_type'   => '',
            'post_parent'      => '',
            'author'	   => '',
            'post_status'      => 'publish',
            'suppress_filters' => true 
        );
        $myposts = get_posts($args); 
        $current_id = get_the_ID();
        foreach ( $myposts as $post ) : setup_postdata( $post );
			if($current_id != $post->ID) {
				get_template_part( 'template-parts/list-product' );
			}
            endforeach; 
            wp_reset_postdata();
        ?>
        </ul>
    </div>
</div>
<script src="<?php echo get_template_directory_uri().'/js/lightbox-plus-jquery.min.js'; ?>"></script>