<?php
/*
  Template Name: contact
 */

?>
<?php get_header(); ?>
<?php get_sidebar(); ?>
<section id="content" class="content">
    <div class="entry-title">
        <h2>Liên hệ</h2>
    </div>
    <div class="form_contact"><?php the_content(); ?></div>
</section>
<?php get_footer(); ?>