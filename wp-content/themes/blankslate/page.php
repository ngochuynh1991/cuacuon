<?php get_header(); ?>
<?php get_sidebar(); ?>
<section id="content" class="content">
    <div class="entry-title">
        <h2><?php the_title(); ?></h2>
    </div>
    <div class="box-body">
        <?php the_content(); ?>
    </div>
</section>
<?php get_footer(); ?>