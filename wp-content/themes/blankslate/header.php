<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri().'/css/lightbox.min.css'; ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
<meta property="fb:app_id" content="1271687472860294" />
<meta property="fb:admins" content="100004539492985"/>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1271687472860294',
      xfbml      : true,
      version    : 'v2.6'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.4";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<div id="wrapper" class="boxed">
    <header>
        <div class="fl">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" style="margin-left: 20px;">
                        <img src="<?php echo get_template_directory_uri().'/images/logo2.png'; ?>" />
                </a>
        </div>
        <div class="header_title fr">
            <h2>NHÀ SẢN XUẤT CỬA LƯỚI AN PHÁT</h2>
            <h3>CHUYÊN CUNG CẤP - LẮP ĐẶT CỬA LƯỚI CHỐNG MUỖI</h3>
            <h4>UY TÍN - CHẤT LƯỢNG - CẠNH TRANH</h4>
        </div>
    </header>
    <nav class="main-menu theme-container">
        <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
        <div class="search-box">
            <?php get_template_part('template-parts/search-form'); ?>
        </div>
    </nav>
    <div id="slider" class="theme-container">
        <?php do_action('slideshow_deploy', '27'); ?>
    </div>
    <div class="main-content theme-container">
    